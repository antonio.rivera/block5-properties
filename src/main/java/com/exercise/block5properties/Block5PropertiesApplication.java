package com.exercise.block5properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class Block5PropertiesApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(Block5PropertiesApplication.class, args);
	}

	/**
	 * Apartado 1: Guardo el valor de las dos variables para mostrarlas
	 */
	@Value("${greeting}")
	private String greeting;
	@Value("${MYURL}")
	private String MYURL;

	@Value("${my.number}")
	private Integer number;

	// Esta variable es la que voy a crear nueva
	@Value("${new.property:new.property no tiene valor}")
	private String newProperty;




	@Autowired
	private Environment env;

	@Bean
	CommandLineRunner apartado3(){
		return args -> {
			System.out.println("El valor de MYURL es: "+env.getProperty("MYURL"));
			System.out.println("El valor de MYURL2 es: "+env.getProperty("MYURL2","NO_tengo_valor"));
		};
	}





	/**
	 * @param args
	 * @throws Exception
	 */

	@Override
	public void run(String... args) throws Exception {
		System.out.println("El valor de greeting es: "+greeting);
		System.out.println("El valor de my.number es: "+number);
		System.out.println("El valor de new.property es: "+newProperty);

	}


}
